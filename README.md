# Experiment Python

> Experiment from December 2020

This is a small project to learn some basics in
[Python](https://docs.python.org/3/). In November 2020, I joined a team at
[beta.gouv](https://beta.gouv.fr/) on a project where we intended to use Python
as the backend language.

You can check out my article to know more about [why I document my process when
I'm testing something new for
me](https://code.likeagirl.io/the-art-of-documenting-your-coding-experience-e868e28cdc45).

## Results 📝
- At first, it was hard to find documentation about how to handle dependencies
  in Python. I use a `requirements.txt` in this project. But now, I find out [Poetry](https://python-poetry.org/), I will use it
  for my next Python projects.
- I learn to use `__init__.py` to [tell Python to use a directory as a package](https://docs.python.org/3/tutorial/modules.html#packages).
- [FastAPI](https://fastapi.tiangolo.com/) is a very smooth and elegant
  framework to easily create API. I like it!
- I'm not used with the `assert` style in tests writing. I should look for more
  test frameworks.

#### Console output: 

![Screenshot_2021-02-06_054220](/uploads/4ac3ddf0d2ebb2a0c7b0f844e999d69d/Screenshot_2021-02-06_054220.png)

#### Browser output on `http://127.0.0.1:8000`:

![Screenshot_2021-02-06_Screenshot_2_](/uploads/291fd689d366b19843a69b7957b674dc/Screenshot_2021-02-06_Screenshot_2_.png)

## Steps

### Install dependencies
- Use a `requirements.txt` file to handle dependencies.
- Add `py_cache` and `*.py[cod]` to `.gitignore`.

### Create API
- Use [FastAPI](https://fastapi.tiangolo.com/) to write a simple API interface.

### Write tests
- Use [Pytest](https://docs.pytest.org/en/stable/) framework to write and run
  tests.
- Use the integrated [Starlette TestClient](https://www.starlette.io/testclient/)
  in [FastAPI](https://fastapi.tiangolo.com/tutorial/testing/) to make requests
  in the application.
- Add a `__init__.py` to enable Python to make import correctly.

## Requirements

- [Python 3.9.1](https://www.python.org/downloads/release/python-391/)

## Usage

Install the dependencies:
```sh
cd api
python -m pip install -r requirements.txt
```

Start the app: 
```sh
uvicorn main:app --reload
```

Or run the test:
```sh
pytest
```

## Ressources

- https://docs.python.org/fr/3/tutorial/
- https://pip.pypa.io/en/stable/user_guide/
- https://blog.usejournal.com/why-and-how-to-make-a-requirements-txt-f329c685181e
- https://fastapi.tiangolo.com/
- https://www.starlette.io/
